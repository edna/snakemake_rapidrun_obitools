##########################################################################
## Codes for scientific papers related to metabarcoding studies
##
## Here, we reproduce the bioinformatics workflow used by SPYGEN 
## We improve its performance by adding parallelization and containerazition features.
## This workflow generates species environmental presence from raw eDNA data. 
## This workflow use the workflow management system SNAKEMAKE.
##
## AUTHORS
## =======
## * Pierre-Edouard Guerin   | pierre-edouard.guerin@cefe.cnrs.fr
## * Virginie Marques        | virginie.marques@etu.umontpellier.fr
## * CNRS/CEFE, CNRS/MARBEC  | Montpellier, France
## * 2018-2020
##
## USAGE
## =====
## CORES=8
## CONFIGFILE=config/config_tutorial_rapidrun.yaml
## bash main.sh $CORES $CONFIGFILE
##
## DESCRIPTION
## ===========
##
## 
##
##########################################################################
CORES=$1
CONFIGFILE=$2
##
#CORES=16
#CONFIGFILE="config/config_laperouse_marbec.yaml"
#CONFIGFILE="config/config_marbec_rapdirun_test.yaml"
#CONFIGFILE="config/config_marbec_rapdirun_test_sdx.yaml"
#CONFIGFILE="01_infos/config_test.yaml"
#CONFIGFILE="01_infos/config_laperouse_alsace.yaml"
#CONFIGFILE="config/config_tutorial_rapidrun.yaml"
#CONFIGFILE="config/config_tutorial_classic.yaml"

#rm -rf results;mkdir results;snakemake --configfile config/config_test_rapidrun.yaml --cores 8 --use-conda

snakemake --configfile $CONFIGFILE --cores $CORES --use-conda

############################################################################### 
#snakemake --configfile "../"$CONFIGFILE -s Snakefile --delete-all-output --dry-run
#snakemake --configfile "../"$CONFIGFILE -s Snakefile --delete-all-output --rerun-incomplete --cores 8
#nohup snakemake --configfile "../"$CONFIGFILE -s Snakefile -j $CORES --use-singularity --singularity-args "--bind /media/superdisk:/media/superdisk" --latency-wait 20 &
#snakemake --configfile "../"$CONFIGFILE -s Snakefile -j $CORES --use-singularity --singularity-args "--bind /media/superdisk:/media/superdisk" --latency-wait 20 --dry-run
