__author__ = ["Pierre-Edouard Guerin", "Morgane Bruno"]
__license__ = "MIT"

## On scattered fastq chunk Paired end alignment then keep reads with quality > 40
rule illuminapairedend:
    input:
        R1 = Path('results', config['subfolders']['fastq_chunk'], '{run}_R1.part_{chunk}.fastq.gz'),
        R2 = Path('results', config['subfolders']['fastq_chunk'], '{run}_R2.part_{chunk}.fastq.gz')
    output:
        temp(Path('results', config['subfolders']['fastq_chunk'], '{run}_{chunk}.fastq'))
    conda:
        '../envs/obitools_envs.yaml'
    container:
        config["singularity"]["obitools"]
    log:
        Path('logs', config['subfolders']['fastq_chunk'], '{run}_{chunk}.log')
    params:
        s_min = config["illuminapairedend"]["s_min"]
    shell:
        '''
        illuminapairedend -r {input.R2} {input.R1} --score-min={params.s_min} > {output} 2> {log}
        '''

## gather `chunk` files into a single `run` fastq file
list_chunk = ["%03d" % i for i in range(1,config['illuminapairedend']['nb_chunk']+1)]
rule merge_chunks:
    input:
        expand(Path('results', config['subfolders']['fastq_chunk'],
         '{{run}}_{chunk}.fastq').as_posix(), chunk = list_chunk)
    output:
        Path('results', config['subfolders']['merge_fastq'], '{run}.fastq')
    log:
        Path('logs', config['subfolders']['merge_fastq'], '{run}.log')
    priority: 50
    shell:
        '''
        cat {input} > {output}
        echo 'Merge {input} in {output}' > {log}
        '''
