__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## Assign each sequence to a taxon
rule taxonomic_assignment:
    input:
        rules.dereplicate_runs.output
    output:
        Path('results', config['subfolders']['taxonomic_assignment'], '{demultiplex}_genbank.tag.fasta')
    threads:
        workflow.cores * 0.5
    conda:
        '../envs/obitools_envs.yaml'
    container:
        config["singularity"]["obitools"]
    log:
        Path('logs', config['subfolders']['taxonomic_assignment'], '{demultiplex}_genbank.tag.log')
    params:
        db = lambda w: config['assign_taxon']['bdr'][str({w.demultiplex}).split('/')[1]],
        fasta = lambda w: config['assign_taxon']['fasta'][str({w.demultiplex}).split('/')[1]]
    shell:
        '''
        mkdir -p $(dirname {output})
        ecotag -d {params.db} -R {params.fasta} {input} > {output} 2> {log}
        '''

if config['custom_baseref']:
    rule custom_bdr_taxonomic_assignment:
        input:
            get_custom_assignment_input
        output:
            Path('results', config['subfolders']['taxonomic_assignment'], '{demultiplex}_custom.tag.fasta')
        threads:
            workflow.cores * 0.5
        conda:
            '../envs/obitools_envs.yaml'
        container:
            config["singularity"]["obitools"]
        log:
            Path('logs', config['subfolders']['taxonomic_assignment'], '{demultiplex}_custom.tag.log')
        params:
            db = lambda w: config['assign_customtax']['bdr'][str({w.demultiplex}).split('/')[1]],
            fasta = lambda w: config['assign_customtax']['fasta'][str({w.demultiplex}).split('/')[1]]
        shell:
            '''
            mkdir -p $(dirname {output})
            ecotag -t {params.db} -R {params.fasta} {input} > {output} 2> {log}
            '''