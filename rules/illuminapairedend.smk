__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## Paired end alignment then keep reads with quality > 40
rule illuminapairedend:
    input:
        R1 = Path('results', config["files"]["folder_fastq"], '{run}_R1.fastq.gz'),
        R2 = Paht('results', config["files"]["folder_fastq"], '{run}_R2.fastq.gz')
    output:
        Path('results', config['subfolders']['merge_fastq'], '{run}.fastq')
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    log:
        Path('logs', config['subfolders']['merge_fastq'], '{run}.log')
    params:
        s_min = config["illuminapairedend"]["s_min"]
    shell:
        '''
        illuminapairedend -r {input.R2} {input.R1} --score-min={params.s_min} > {output} 2> {log}
        '''