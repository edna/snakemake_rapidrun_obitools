__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


### Clean the sequences for PCR/sequencing errors (sequence variants)
rule clean_pcrerr_samples:
    input:
        rules.goodlength_samples.output
    output:
        temp(Path('results', config['subfolders']['clean_pcrerror'], '{demultiplex}/{sample}.r.l.u.fasta'))
    conda:
        '../envs/obitools_envs.yaml'
    container:
        config["singularity"]["obitools"]
    log:
        Path('logs', config['subfolders']['clean_pcrerror'], '{demultiplex}/{sample}.r.l.u.logs')
    message: '(obiclean) Clean PCR/sequencing errors: {wildcards.demultiplex}/{wildcards.sample}'
    params:
        r = config["clean_pcrerr_samples"]["r"]
    shell:
        '''
        mkdir -p $(dirname {output})
        if [ -s {input} ]; then
		obiclean -r {params.r} {input} > {output} 2> {log}
        else
		touch {output}
		echo "warnings :: empty input :: {input}" > {log}
        fi
        '''
