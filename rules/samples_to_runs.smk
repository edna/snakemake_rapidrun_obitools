__author__ = 'Morgane Bruno'
__licence__ = 'MIT'

rule samples_to_runs:
    input:
       get_internal_samples
    output:
        Path('results', config['subfolders']['pool_sequences'], '{demultiplex}.fasta')
    log:
        Path('logs', config['subfolders']['pool_sequences'], '{demultiplex}.log')
    message:
        'Pool sample sequences in {wildcards.demultiplex}'
    shell:
        '''
        mkdir -p $(dirname {output})
        if [ -z "{input}" ]; then
            touch {output}
            echo 'No samples in {output}' > {log}
        else
            cat {input} > {output}
            echo 'Merge samples in {output}' > {log}
        fi
        '''
