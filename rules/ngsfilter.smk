__author__ = 'Morgane Bruno'
__licence__ = 'MIT'

## Create file describing demulitplex data
rule demulti__demultiplex_data:
    input:
        config['files']['demultiplex']
    output:
        Path('results', config['subfolders']['settings'], 'all_demultiplex.csv')
    log:
        Path('logs', config['subfolders']['settings'], 'all_demultiplex.log')
    conda:
        '../envs/env_scripts_python.yaml'
    params:
        dir_primers_tags = Path('results', config['subfolders']['demultiplex_tag']),
        blackl = json.loads(json.dumps(config['blacklist'])),
        dat = json.loads(json.dumps(config['files']['dat']))
    message:
        'Create file describing demulitplex data'
    script: '../scripts/write_demultiplex_files.py'

## Assign each sequence record to the corresponding sample/marker combination:
rule demulti__ngsfilter:
    input:
        fq = get_input_ngsfilter,
        demulti = rules.demulti__demultiplex_data.output
    output:
        temp(Path('results', config['subfolders']['demultiplex_tag'], '{demultiplex}.ali.assigned.fastq'))
    log:
        Path('logs', config['subfolders']['demultiplex_tag'], '{demultiplex}.ali.assigned.log')
    conda:
        '../envs/obitools_envs.yaml'
    container:
        config['singularity']['obitools']
    params:
        in_tsv = get_one_pmr
    message:
        'Assign each sequence record: {wildcards.demultiplex}'
    shell:
        '''
        ngsfilter -t {params.in_tsv} -u $(dirname {output})/unidentified.fastq {input.fq} --fasta-output > {output} 2> {log}
        '''
