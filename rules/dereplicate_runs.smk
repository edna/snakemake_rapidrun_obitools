__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## Dereplicate and merge samples together
rule dereplicate_runs:
    input:
        rules.samples_to_runs.output
    output:
        Path('results', config['subfolders']['dereplicate_runs'], '{demultiplex}.uniq.fasta')
    conda:
        '../envs/obitools_envs.yaml'
    container:
        config["singularity"]["obitools"]
    log:
        Path('logs', config['subfolders']['dereplicate_runs'], '{demultiplex}.uniq.log')
    shell:
        '''
        mkdir -p $(dirname {output})
        obiuniq -m sample {input} > {output} 2> {log}
        '''
