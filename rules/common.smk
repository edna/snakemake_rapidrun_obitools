__author__ = "Morgane Bruno"
__licence__ = "MIT"

# --------------------------------- Functions ---------------------------------#
### ecotag - taxonomic assignement
def get_custom_assignment_input(wildcards):
    with open(config['files']['demultiplex'], 'r') as f:
        for line in f:
            line_splitted = line.strip('\n').split(';')
            projet = line_splitted[3]
            marker = line_splitted[4]
            run = line_splitted[1]
            pmr = f'{projet}/{marker}/{run}'
            if marker in config['assign_customtax']['bdr']:
                if pmr in {wildcards.demultiplex}:
                    return  Path('results', config["subfolders"]["dereplicate_runs"], f'{pmr}.uniq.fasta')
### ngsfilter
def get_one_pmr(wildcards):
    with open(config['files']['demultiplex'], 'r') as f:
        for line in f:
            line_splitted = line.strip('\n').split(';')
            projet = line_splitted[3]
            marker = line_splitted[4]
            run = line_splitted[1]
            if f'{projet}/{marker}/{run}' in {wildcards.demultiplex}:
                return  Path('results', config['subfolders']["demultiplex_tag"], f'{projet}_{marker}_{run}.tsv')

def get_input_ngsfilter(wildcards):
    with open(config['files']['demultiplex']) as f:
        demulti = str({wildcards.demultiplex})
        w_run =  demulti.split("/")[2]
        for line in f:
            line_splitted = line.strip('\n').split(';')
            run = line_splitted[1]
            if run in w_run:
                return Path('results', config['subfolders']["remove_unaligned"], f'{run}.ali.fastq')

def get_internal_samples(wildcards):
    checkpoint_output = checkpoints.demulti__obisplit.get(**wildcards).output[0]
    return expand('results/{subf}/{demultiplex}/{sample}.c.r.l.u.fasta',
            subf = config['subfolders']["remove_internal"],
            demultiplex = wildcards.demultiplex,
            sample = glob_wildcards(os.path.join(checkpoint_output, '{sample}.fasta')).sample)

### Targets

def get_targets():
    files = []
    with open(config['files']['demultiplex'], 'r') as f:
        for line in f:
            line_splitted = line.strip('\n').split(';')
            projet = line_splitted[3]
            marker = line_splitted[4]
            run = line_splitted[1]
            demultiplex = f'{projet}/{marker}/{run}'
            if projet not in config['blacklist']['projet'] and run not in config['blacklist']['run'] and f'results/{demultiplex}_{projet}_{marker}_genbank_ecotag.csv' not in files:
                files.append(f'results/{demultiplex}_{projet}_{marker}_genbank_ecotag.csv')
                if config['custom_baseref'] and marker in config['assign_customtax']['bdr'] and f'results/{demultiplex}_{projet}_{marker}_custom_ecotag.csv' not in files:
                    files.append(f'results/{demultiplex}_{projet}_{marker}_custom_ecotag.csv')
    return files