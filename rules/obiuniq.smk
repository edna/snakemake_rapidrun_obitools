__author__ = "Morgane Bruno"
__licence__ = "MIT"

## dereplicate reads into uniq sequences
rule dereplicate__obiuniq:
    input:
        Path('results', config['subfolders']['split_fasta'], '{demultiplex}/{sample}.fasta')
    output:
        temp(Path('results', config['subfolders']['dereplicate_sample'], '{demultiplex}/{sample}.uniq.fasta'))
    log:
        Path('logs', config['subfolders']['dereplicate_sample'], '{demultiplex}/{sample}.uniq.log')
    conda:
        '../envs/obitools_envs.yaml'
    container:
        config['singularity']['obitools']
    priority: 50
    message:
        'dereplicate reads into uniq sequences: {wildcards.demultiplex}/{wildcards.sample}'
    shell:
        '''
        mkdir -p $(dirname {output})
        obiuniq -m sample {input} > {output} 2> {log}
        '''
