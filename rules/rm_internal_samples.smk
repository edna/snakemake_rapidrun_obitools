__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"

## Remove sequence which are classified as 'internal' by obiclean
rule rm_internal_samples:
    input:
        rules.clean_pcrerr_samples.output
    output:
        Path('results', config['subfolders']['remove_internal'],'{demultiplex}/{sample}.c.r.l.u.fasta')
    conda:
        '../envs/obitools_envs.yaml'
    container:
        config["singularity"]["obitools"]
    log:
        Path('logs', config['subfolders']['remove_internal'],'{demultiplex}/{sample}.c.r.l.u.log')
    shell:
        '''
        mkdir -p $(dirname {output})
        obigrep -p "obiclean_internalcount == 0" {input} > {output} 2> {log}
        '''
