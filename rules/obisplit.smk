__author__ = "Morgane Bruno"
__licence__ = "MIT"

## Split the input file in a set of subfiles according to the values of attribute 'sample'
checkpoint demulti__obisplit:
    input:
        rules.demulti__ngsfilter.output
    output:
        directory(Path('results', config['subfolders']['split_fasta'], '{demultiplex}'))
    log:
        Path('logs', config['subfolders']['split_fasta'], '{demultiplex}_obisplit.log')
    conda:
        '../envs/obitools_envs.yaml'
    container:
        config['singularity']['obitools']
    message:
        'Split the input file in a set of subfiles: {wildcards.demultiplex}'
    resources:
        bigfile = 1
    shell:
        '''
        mkdir -p {output}
        obisplit -p {output}/ -t sample --fasta {input} &> {log}
        '''
