__author__ = "Morgane Bruno"
__license__ = "MIT"

list_chunk = ["%03d" % i for i in range(1,config['illuminapairedend']['nb_chunk']+1)]

rule split_fastq:
    input:
        R1 = Path(config["files"]["folder_fastq"], '{run}_R1.fastq.gz'),
        R2 = Path(config["files"]["folder_fastq"], '{run}_R2.fastq.gz')
    output:
        R1 = temp(expand(Path('results', config['subfolders']['fastq_chunk'],
         '{{run}}_R1.part_{chunk}.fastq.gz').as_posix(), chunk = list_chunk)),
        R2 = temp(expand(Path('results', config['subfolders']['fastq_chunk'],
         '{{run}}_R2.part_{chunk}.fastq.gz').as_posix(), chunk = list_chunk))
    log:
        Path('logs', config['subfolders']['fastq_chunk'], '{run}.log')
    conda:
        '../envs/seqkit_envs.yaml'
    params:
        nb_chunk = config['illuminapairedend']['nb_chunk'],
        out_dir = Path('results', config['subfolders']['fastq_chunk'])
    shell:
        '''
        seqkit split2 -1 {input.R1} -2 {input.R2} -p {params.nb_chunk} -O {params.out_dir} &> {log}
        '''
