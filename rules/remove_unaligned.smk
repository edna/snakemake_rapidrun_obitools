__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"

## Remove unaligned sequence records
rule remove_unaligned:
    input:
        Path('results', config['subfolders']['merge_fastq'], '{run}.fastq')
    output:
        Path('results', config['subfolders']['remove_unaligned'], '{run}.ali.fastq')
    conda:
        '../envs/obitools_envs.yaml'
    container:
        config["singularity"]["obitools"]
    log:
        Path('logs', config['subfolders']['remove_unaligned'], '{run}.ali.log')
    shell:
        '''
        obigrep -p 'mode!=\"joined\"' {input} > {output} 2> {log}
        '''
