__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## only sequence more than 20bp with no ambiguity IUAPC with total coverage greater than 10 reads
rule goodlength_samples:
    input:
        rules.dereplicate__obiuniq.output
    output:
        temp(Path('results', config['subfolders']['goodlength_samples'],'{demultiplex}/{sample}.l.u.fasta'))
    conda:
        '../envs/obitools_envs.yaml'
    container:
        config["singularity"]["obitools"]
    log:
        Path('logs', config['subfolders']['goodlength_samples'],'{demultiplex}/{sample}.l.u.log')
    message: '(obigrep) Keep only sequence > 20pb without IUPAC ambiguity: {wildcards.demultiplex}/{wildcards.sample}'
    priority: 50
    params:
        seq_count=config["good_length_samples"]["seq_count"],
        seq_length_min=config["good_length_samples"]["seq_length_min"]
    shell:
        '''
        mkdir -p $(dirname {output})
        obigrep  -p "count>{params.seq_count}" -s "^[ACGT]+$" -p "seq_length>{params.seq_length_min}" {input} > {output} 2> {log}
        '''
