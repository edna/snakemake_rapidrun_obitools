__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## Generate a table final results
rule table_assigned_sequences:
    input:
        rules.sort_abundance_assigned_sequences.output
    output:
        Path('results', '{demultiplex}_{projet}_{marker}_{bdr}_ecotag.csv')
    log:
        Path('logs', '{demultiplex}_{projet}_{marker}_{bdr}_ecotag.csv')
    conda:
        '../envs/obitools_envs.yaml'
    container:
        config["singularity"]["obitools"]
    shell:
        '''
        obitab -o {input} > {output} 2> {log}
        '''
