## concatenate samples into run
for projet in `ls results/12_rm_internal_samples/`;
do 
 for marker in `ls results/12_rm_internal_samples/${projet}/`;
 do
  for run in `ls results/12_rm_internal_samples/${projet}/${marker}/`;
  do
  echo results/13_cat_samples_into_runs/${projet}/${marker}/${run};
  mkdir -p results/13_cat_samples_into_runs/${projet}/${marker}/
  cat results/12_rm_internal_samples/${projet}/${marker}/${run}/*.c.r.l.u.fasta > results/13_cat_samples_into_runs/${projet}/${marker}/${run}.fasta
  done
 done
done