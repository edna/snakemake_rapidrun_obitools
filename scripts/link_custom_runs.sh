CUSTOMPROJMARKRUNFILE=$1
INFOLDER="results/13_cat_samples_into_runs/"

while IFS=$'\t' read -r -a myArray
do
 echo "${INFOLDER}""${myArray[1]}" "${INFOLDER}""${myArray[0]}"
 ln -s "${PWD}""/""${INFOLDER}""${myArray[1]}"".fasta" "${INFOLDER}""${myArray[0]}"".fasta"
done < ${CUSTOMPROJMARKRUNFILE}
