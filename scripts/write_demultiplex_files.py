#!/usr/bin/env python3
__author__ = "Morgane Bruno"
__licence__ = "MIT"

#_libs
import pandas as pd
import sys, os, warnings, logging
from Bio.Seq import Seq
from pathlib import Path


#_main
def main(argv) -> None:
    #args
    all_samples = snakemake.input[0]
    all_demultiplex = snakemake.output[0]   
    dir_primers_tags = snakemake.params.dir_primers_tags   #results/demultiplexing/
    blacklist = snakemake.params.blackl  #config['blacklist']
    dat = snakemake.params.dat  #config['files']['dat']
    log = snakemake.log[0]   #log file

    #logs
    logging.basicConfig(format = '%(asctime)s :: %(levelname)s :: %(message)s',
                        filename = log,
                        filemode = 'w',
                        level = logging.INFO)
    logging.captureWarnings(True)

    df_samples = read_samples_csv(all_samples, blacklist)
    df_demulti = write_demultiplex_csv(df_samples, all_demultiplex, dat)
    write_primers_tags_tsv(df_samples, df_demulti, dir_primers_tags)

    logging.shutdown()


#_fun
def read_samples_csv(file_path: str, blacklist: dict) -> pd.pandas.core.frame.DataFrame:
    ''' Read the all_samples.csv file and remove 'blacklisted' projets/runs

    Args:
        file_path (str): path to all_samples.csv file
        blacklist (dict): blacklisted projets/runs

    Returns:
        Pandas DataFrame: a dataframe with metadata ('plaque', 'run', 'sample', 'projet', 'marker')
    '''
    df_samples = pd.read_csv(file_path, sep = ';', header = None)
    df_samples.columns = ['plaque', 'run', 'sample', 'projet', 'marker']

    # remove blacklisted
    blackl_projet = blacklist['projet']
    blackl_run = blacklist['run']
    return df_samples[df_samples.projet.isin(blackl_projet) | df_samples.run.isin(blackl_run) == False]


def is_primer_sequence(primer: str, alphabet: str = 'ATGCRYSWKMBDHVN') -> list:
    ''' Check primer sequence

    Args:
        primer (str): primer sequence
        alphabet (str): dna nucleotides and nucleotide ambiguity code

    Returns:
        boolean list: for each character in the sequence: True if the character is in the alphabet
    '''
    return [c in alphabet for c in primer]


def read_dat(file_dat: str) -> pd.pandas.core.frame.DataFrame:
    ''' Read and check the .dat file

    Args:
        file_dat (str): .dat file path

    Returns:
        Pandas DataFrame: dataframe with marker data (experiment, plaque position, primers, barcode)
    '''
    # check if the .dat file has 6 columns
    df_dat = pd.read_csv(file_dat, sep = '\t', header = None)
    assert df_dat.shape[1] == 6
    df_dat.columns = ['experiment', 'plaque', 'barcode', 'primer5', 'primer3', 'F']

    # check primers
    for col_name in ['primer5', 'primer3']:
        is_primer = [is_primer_sequence(seq) for seq in df_dat[col_name]]
        for p in is_primer:
            assert False not in p
    return df_dat


def write_primers_tags_tsv(samples: pd.pandas.core.frame.DataFrame, demultiplex: pd.pandas.core.frame.DataFrame, out_dir: str) -> None:
    ''' Write the files describing the primers and tags used to assign each sequence record to the corresponding sample/marker combination

    Args:
        samples (Pandas DataFrame): dataframe all_samples
        demultiplex (Pandas DataFrame): dataframe all_demultiplex
        out_dir (str): the folder in which the output files will be store
    '''
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)
    df_pmr = samples.loc[:, ('projet', 'marker', 'run')]
    df_pmr['run_marker'] = df_pmr.run + '/' + df_pmr.marker
    df_pmr['proj_marker'] = df_pmr.projet + '/' + df_pmr.marker
    df_pmr['proj_mark_run'] = df_pmr.proj_marker + '/' + df_pmr.run
    df_pmr.drop_duplicates()
    pmr_files = []
    for pmr in df_pmr.proj_mark_run.unique():
        projet = pmr.split('/')[0]
        marker = pmr.split('/')[1]
        run = pmr.split('/')[2]
        df = demultiplex[(demultiplex.projet == projet) & (demultiplex.marker == marker) & (demultiplex.run == run)]
        if not df.empty:
            file_name = Path(out_dir, f'{projet}_{marker}_{run}.tsv')
            pmr_files.append(file_name)
            logging.info(f'Writting file describing primers and tags for projet {projet}, marker {marker}: {file_name}')
            dat = {
                '#exp': list(df['projet']),
                'sample': list(df['sample']),
                'tags': list(df['barcode5']),
                'forward_primer': list(df['primer5']),
                'reverse_primer': list(df['primer3']),
                'extra_information': list(pd.Series('F').repeat(len(df.projet)))
            }
            df_dat = pd.DataFrame(dat)
            df_dat.to_csv(file_name, index=False, sep='\t')
        else:
            warnings.warn('WARNING: Unable to write the file describing primers and tags for projet {projet}, marker {marker}')


def write_demultiplex_csv(samples: pd.pandas.core.frame.DataFrame, out_file: str, dat: dict) -> pd.pandas.core.frame.DataFrame:
    ''' Write all_demultiplex.csv file

    Args:
        samples (Pandas DataFrame): dataframe all_samples
        out_file (str): folder/all_demultiplex.csv
        dat (dict): marker.dat files directory

    Returns:
        Pandas DataFrame: dataframe with demultiplex data
    '''
    uniq_run = samples.run.unique()
    uniq_projet = samples.projet.unique()
    uniq_marker = samples.marker.unique()
    # create a marker dict: key = marker and value = Pandas DataFrame describing marker data
    marker_dat = {}
    for marker in uniq_marker:
        marker_dat[marker] = read_dat(dat[marker])
    # init demultiplex DataFrame
    df_demulti = pd.DataFrame(columns = ['demultiplex', 'projet', 'marker', 'run', 'plaque', 'sample', 'barcode5', 'barcode3', 'primer5', 'primer3', 'min_f', 'min_r', 'lenBarcode5', 'lenBarcode3'])

    for run in uniq_run:
        for marker in uniq_marker:
            df = samples[(samples.run == run) & (samples.marker == marker)]
            if not df.empty:
                for plaque in df.plaque:
                    row = df[df.plaque == plaque]
                    projet = row['projet'].values[0]
                    marker = row['marker'].values[0]
                    sample = row['sample'].values[0]
                    plaque_dat = marker_dat[marker].loc[marker_dat[marker]['plaque'] == plaque]
                    b5 = plaque_dat.barcode.values[0]
                    b3 = str(Seq(b5).reverse_complement())
                    p5 = plaque_dat.primer5.values[0]
                    p3 = plaque_dat.primer3.values[0]
                    one_row = {
                        'demultiplex': f'{projet}/{marker}/{sample}',
                        'projet': projet,
                        'marker': marker,
                        'run': run,
                        'plaque': plaque,
                        'sample': sample,
                        'barcode5': b5,
                        'barcode3': b3,
                        'primer5': p5,
                        'primer3': p3,
                        'min_f': int(len(p5) * 2/3),
                        'min_r': int(len(p3) * 2/3),
                        'lenBarcode5': len(b5),
                        'lenBarcode3': len(b3)
                    }
                    df_demulti = df_demulti.append(one_row, ignore_index = True)
    df_demulti.to_csv(out_file, index = None, header = True)
    return df_demulti

if __name__ == '__main__':
    main(sys.argv)
