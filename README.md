## eDNA-seq Metabarcoding annotation pipeline

**eDNA-seq Metabarcoding annotation** is a bioinformatics pipeline built using Snakemake, a workflow tool to run and manage tasks in any execution environment.

## Introduction

**eDNA-seq Metabarcoding annotation** is specifically used for the analysis of environmental DNA metabarcoding NGS data, demultiplexing, filtering, dereplicating and taxonomic assignment (NCBI taxonomy and custom taxonomy).

This pipeline has been initially tested with marine environmental DNA samples, using molecular markers such as Vert01 ([Riaz et al. 2011](https://doi.org/10.1093/nar/gkr732)), Teleo01 ([Valentini et al. 2016](https://doi.org/10.1111/mec.13428)), Chond01 or Mamm01 ([Taberlet et al. 2018](https://doi.org/10.1093/oso/9780198767220.001.0001)). The workflow should work with any organisms and environment. It is proven for large-scale data analysis.




## Method


This workflow performs step by step analysis of metabarcoding data using [obitools](https://pythonhosted.org/OBITools/index.html).

The workflow assembles paired-end reads together (illuminapairedend), applies demultiplexing (ngsfilter), dereplicates sequences (obiuniq), detect and removes chimera and PCR clones (obiclean; obigrep) and assigns taxonomy to each representative sequences (NCBI taxonomy and custom taxonomy; ecotag). Ultimately, abundance distribution among samples of representative sequences is generated as a table.


## Workflow 


![workflow obitools](https://gitlab.mbb.univ-montp2.fr/edna/snakemake_only_obitools/-/raw/master/schema_only_obitools.png)

## Quick Start

See [Installion section](https://gitlab.mbb.univ-montp2.fr/edna/snakemake_rapidrun_obitools/-/wikis/Tutorial#1-installation) for installation instructions.


Download example data with:

```
curl -JLO http://gitlab.mbb.univ-montp2.fr/edna/snakemake_rapidrun_data_test/-/raw/master/test_rapidrun_data.tar.gz; tar zfxz test_rapidrun_data.tar.gz -C resources/test/
```

* `./resources/test/test_rapidrun_data/`: this folder contains a reference database for 4 markers (Teleo01; Mamm01; Vert01; Chond01), NGS metabarcoding raw data, required metadata to handle demultiplexing on RAPIDRUN format. 

If you have installed **eDNA-seq Metabarcoding annotation**, you can run the example data with:


```
snakemake --configfile config/config_template.yaml --cores 4 --use-conda
```

This will generate outputs into `./results` folder.

Final tables are available as:

```
results/projet1_chond_run1_ecotag_ncbi.csv   results/projet1_teleo_run5_ecotag_ncbi.csv   results/projet1_chond_run2_ecotag_ncbi.csv
results/projet1_teleo_run2_ecotag_ncbi.csv   results/projet1_vert_run3_ecotag_ncbi.csv    results/projet1_mamm_run4_ecotag_ncbi.csv
```

## Next steps

Now that you've gotten the example to work, see the [wiki](https://gitlab.mbb.univ-montp2.fr/edna/snakemake_rapidrun_obitools/-/wikis/Home) to navigate to the more detailed descriptions and instructions for exploring your own data.


## Software Requirements

* python3
* python3 required modules included: snakemake

## Credits

**eDNA-seq Metabarcoding annotation**  was coded and written by Virginie Marques and Pierre-Edouard Guerin and Morgane Bruno.

We thank the following people for their help in the development of this pipeline: Agnes Duhamet, Alice Valentini, Apolline Gorry, Bastien Mace, David Mouillot, Emilie Boulanger, Laetitia Mathon, Laura Benestan, Stephanie Manel, Tony Dejean.


## Contributions and Support

:bug: If you are sure you have found a bug, please submit a bug report. You can submit your bug reports on Gitlab [here](https://gitlab.mbb.univ-montp2.fr/edna/snakemake_rapidrun_obitools/-/issues).


