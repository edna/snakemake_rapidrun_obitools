#===============================================================================
#HEADER
#===============================================================================
__author__ = ["Pierre-Edouard Guerin", "Morgane Bruno"]
__credits__ = ["Pierre-Edouard Guerin", "Virginie Marques", "Morgane Bruno"]
__license__ = "MIT"
__version__ = "1.3"
__maintainer__ = "Morgane Bruno"
__email__ = "morgane.bruno@cefe.cnrs.fr"
__status__ = "Production"
"""
Codes for scientific papers related to metabarcoding studies

AUTHORS
=======
* Morgane Bruno           | morgane.bruno@cefe.cnrs.fr
* Pierre-Edouard Guerin   | pierre-edouard.guerin@cefe.cnrs.fr
* Virginie Marques        | virginie.marques@cefe.cnrs.fr
* CNRS/CEFE, CNRS/MARBEC  | Montpellier, France
* 2018-2021


DESCRIPTION
===========
This is a Snakefile using SNAKEMAKE workflow management system
From sample description files .dat files, config.yaml, rapidrun.tsv
it will return a demultiplexing.csv file. The output contains all required
wildcards en related information necessary to run the next workflow steps.
"""

from snakemake.utils import min_version
min_version("6.10.0")
import os, csv, json

###############################################################################
# WORKFLOW
###############################################################################

#_rules
include: "rules/common.smk"
# Merge paired-end
if config['illuminapairedend']['nb_chunk'] != 0:
    include: "rules/split_fastq.smk"
    include: "rules/chunk_illuminapairedend.smk"
else:
    include: "rules/illuminapairedend.smk"
include: "rules/remove_unaligned.smk"
# Demultiplexing
include: "rules/ngsfilter.smk"
# Filtering
include: "rules/obisplit.smk"
include: "rules/obiuniq.smk"
include: "rules/goodlength_samples.smk"
include: "rules/clean_pcrerr_samples.smk"
include: "rules/rm_internal_samples.smk"
include: "rules/samples_to_runs.smk"
include: "rules/dereplicate_runs.smk"
# Taxonomic assignment
include: "rules/taxonomic_assignment.smk"
include: "rules/remove_annotations.smk"
include: "rules/sort_abundance_assigned_sequences.smk"
include: "rules/table_assigned_sequences.smk"

#_targets
rule all:
    input:
        get_targets()
